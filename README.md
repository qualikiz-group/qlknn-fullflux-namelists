This repository contains auto-generated FORTRAN namelists from the
[QLKNN JSON files](https://gitlab.com/qualikiz-group/qlknn-fullflux) for
use in [QLKNN-fortran](https://gitlab.com/qualikiz-group/QLKNN-fortran).
Please respect the LICENSEs and READMEs from those repositories.